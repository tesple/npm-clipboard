This library allows you to copy data to clipboard directly from string variable or from selectable HTML element.

# Usage

Installation

```$ npm install ez-clipboard```

Sample usage - copy text

```
import ezClipboard from 'ez-clipboard';

...

ezClipboard.copyPlain('YOUR_TEXT_HERE');
```

Sample usage - copy on button blick from textarea

```
const setupButton = () => { 
    const button = document.createElement("BUTTON");        
    const buttonText = document.createTextNode("Copy from textarea");

    textArea.style.position = 'fixed';
    button.style.top = 0;
    button.style.left = 0;
    button.appendChild(buttonText);
    document.body.appendChild(button);  

		return button;
};


const setupTextArea = (textToCopy = '') => {
    const textArea = document.createElement("textarea");
    textArea.value = textToCopy;
 
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.right = 0;
    textArea.style.width = '350px';
    textArea.style.height = '200px';
    document.body.appendChild(textArea);

    return textArea;
};

const button = setupButton();
const textArea = setupTextArea();
const onClickHandler = () => ezClipboard.copyFromElement(textArea);

button.addEventListener('click', onClickHandler);
```