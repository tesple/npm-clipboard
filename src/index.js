const ezClipboard = {};

const getStatusObject = (isSuccess = false, stringValue = null) => ({
    success: isSuccess,
    value: stringValue
});

ezClipboard.__setupTextArea__ = (textToCopy = '') => {
    const textArea = document.createElement("textarea");
    textArea.value = textToCopy;
    
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.right = 0;
    textArea.style.width = 0;
    textArea.style.height = 0;

    document.body.appendChild(textArea);
  
    return textArea;
};

ezClipboard.copyPlain = (variable = '', resultCallback = () => {}) => {
    let isSuccess = false;
    let status = null;
    const textArea = ezClipboard.__setupTextArea__(variable); 
    textArea.select();
 
    try {
        isSuccess = document.execCommand('copy');
        status = getStatusObject(isSuccess, variable);
    } catch (err) {
        status = getStatusObject();
    }
    document.body.removeChild(textArea);
    resultCallback(status);
    
    return status;
} 

ezClipboard.copyFromElement = (element = null, resultCallback = () => {}) => {
    let isSuccess = false;
    let status = null;
    element.select();
 
    try {
        isSuccess = document.execCommand('copy');
        status = getStatusObject(isSuccess, element);
    } catch (err) {
        status = getStatusObject(isSuccess, element);
    }
} 


export default ezClipboard;